#------------------------------------------------------------------------------
# Author        : Chandra Deep Tiwari
# Date          : 19/02/2019
# Version       : 0.1
# Review By     :     
# _______________
# Change History
# ```````````````
# Editor Name         Version  Date         Description
# _____________________________________________________________________________
# C.D.Tiwari          0.1      19/02/18     First version, 
#
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# Base Image Section
#------------------------------------------------------------------------------
FROM cdtiwari/ebi-biopackages.base:0.1

#------------------------------------------------------------------------------
# ROOT MODE
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# Setup Miniconda with Python3
#------------------------------------------------------------------------------
RUN cd /tmp \
    && curl -O https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh \
    && bash Miniconda3-latest-Linux-x86_64.sh -b -p /opt/conda
ENV PATH=/opt/conda/bin:$PATH
RUN conda config --add channels defaults \
    && conda config --add channels conda-forge \
&& conda config --add channels bioconda
