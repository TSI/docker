# Docker files collection
Here you can related docker files templates for bioinformatics uses.
Incorporated best practices of docker file 
Following docker images are also hosted on docker hub [Docker Hub](https://hub.docker.com/)



## Bioconda docker images
* ebi-biopackages.base
* ebi-biopackages.bioconda (default with python3)

      bioconda/
        Dockerfile.base
        Dockerfile.py2    # For python2
        Dockerfile.py3    # For python3 


Maintained by [Cloud Consultancy Team][1], Tips and tricks with Docker [here][3]

Documentation can be found [Cloud Consultancy Team docs][2]

[1]: https://www.ebi.ac.uk/seqdb/confluence/display/TSI/Cloud+Consultancy+Team 
[2]: https://tsi-ccdoc.readthedocs.io/en/master/
[3]: https://tsi-ccdoc.readthedocs.io/en/master/Tech-tips/Tips-and-tricks-with-docker.html
